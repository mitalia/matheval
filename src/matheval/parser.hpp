#ifndef MATHEVAL_PARSER_HPP
#define MATHEVAL_PARSER_HPP
#include <stdint.h>
#include <string>
#include <unordered_map>
#include <stdexcept>
#include "ast.hpp"

#ifdef __GNUC__
#define NORETURN __attribute__((noreturn))
#endif

class ParseError : public std::runtime_error {
    static std::string mkMessage(
            const std::string &message,
            const std::string &expr,
            int position) {
        std::string ret = "Parse error at position "+ std::to_string(position);
        if(expr.size()) {
            ret+= "\n" + expr + "\n" + std::string(position, ' ') +
                + "^";
        }
        ret += "\n" + message;
        return ret;
    }

public:
    std::string message;
    std::string expr;
    int position;

    ParseError(const std::string &message,
               const std::string &expr,
               int position) :
        std::runtime_error(mkMessage(message, expr, position)),
        message(message), expr(expr), position(position) {}
};

struct Function {
    int arity;
    bool pure;
    virtual double call(double args[])=0;
    Function(int arity, bool pure);
    virtual ~Function();
};

class ParserBase {
protected:
    std::string expr;
    int pos=0;

    enum TokenType {
        Number,
        Identifier,
        Special,
        Eof
    };

    TokenType curTokT = Special;
    std::string curToken;

    void nextToken();

    void err(const std::string &message, int pos=-1) NORETURN;

    ParserBase(const std::string &expr);
};

struct CompileContext {
    std::unordered_map<std::string, Function *> functions;
    CompileContext();
};

struct EvalContext {
    typedef std::unordered_map<std::string, double> varsT;
    varsT vars;
};

class EvaluatingParser : public ParserBase {

    double evalSubexpr(int exprPriority);

public:
    EvalContext evalContext;
    CompileContext compileContext;

    EvaluatingParser(const std::string &expr);

    double eval();

    static double eval(const std::string &expr, const EvalContext &context=EvalContext()) {
        EvaluatingParser p(expr);
        p.evalContext = context;
        return p.eval();
    }
};

class ASTParser : public ParserBase {
    ASTPtr evalSubexpr(int exprPriority);
public:
    ASTParser(const std::string &expr);

    ASTPtr parse();
};

class ASTEvaluator {
public:
    EvalContext evalContext;
    CompileContext compileContext;
    ASTPtr ast;
    std::string expr;

    ASTEvaluator(ASTPtr ast, std::string expr="") : ast(ast), expr(std::move(expr)) {}
    double eval();
};

enum Opcode : uint8_t {
    Nop,
    Pop,
    Load,
    LoadVar,
    CallFn,
    Neg,
    Add,
    Sub,
    Mul,
    Div,
    Mod,
    Pow,
    And,
    Or,
    Xor,
    Not,
    LNot,
    LAnd,
    LOr,
    Lt,
    Le,
    Eq,
    Ge,
    Gt,
    Ne,
    Shl,
    Shr
};

struct CompiledCode {
    std::vector<uint8_t> code;
    std::vector<std::string> var_ids;
    std::vector<Function *> funcs;
    int stack_size=0;
};

typedef std::shared_ptr<CompiledCode> CodePtr;

struct RunContext {
    std::vector<double> vars;
    std::unordered_map<std::string, int> name2id;
    CodePtr code;
    std::vector<double> stack;

    RunContext(const EvalContext &ctx, CodePtr code);

    int varId(const std::string &varName) {
        auto it = name2id.find(varName);
        if(it==name2id.end()) return -1;
        return it->second;
    }

    double run();
};

class ASTCompiler : ASTVisitor {
    std::unordered_map<std::string, uint16_t> fnmap;
    std::unordered_map<std::string, uint16_t> varmap;
    CodePtr out;
public:
    CompileContext compileContext;
    ASTPtr ast;
    std::string expr;

    ASTCompiler(ASTPtr ast, std::string expr="") : ast(ast), expr(std::move(expr)) {}
    CodePtr compile();

    // ASTVisitor interface
private:
    void visit(UnaryOperator &op);
    void visit(BinaryOperator &op);
    void visit(Literal &l);
    void visit(FunctionCall &fc);
    void visit(GetVariable &gv);

    void push_code(Opcode opcode);
    void push_val(double val);
    void push_uint16(uint16_t val);
    void update_stack_size(int inc);

    int stack_size;

    void err(const std::string &message, int pos) NORETURN {
        throw ParseError(message, expr, pos);
    }
};


#endif
