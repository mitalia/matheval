#ifndef AST_HPP
#define AST_HPP
#include <memory>
#include <vector>
#include <string>

class ASTBase;
struct ASTVisitor;

typedef std::shared_ptr<ASTBase> ASTPtr;
typedef int PosT;

std::string safeRepr(ASTPtr &node);

struct ASTBase {
    PosT sourcePos = -1;
    std::vector<ASTPtr> children;

    ASTBase(PosT sourcePos) : sourcePos(sourcePos) {}
    virtual void visit(ASTVisitor &visitor)=0;
    virtual ~ASTBase() {}

    virtual std::string repr();
    virtual std::string reprBase()=0;
};

struct UnaryOperator : ASTBase {
    std::string op;

    UnaryOperator(PosT sourcePos, std::string op, ASTPtr subexpr = nullptr);
    std::string reprBase();
    virtual void visit(ASTVisitor &visitor);
    ASTPtr &subexpr() { return children[0]; }
};

struct BinaryOperator : ASTBase {
    std::string op;

    BinaryOperator(PosT sourcePos, std::string op, ASTPtr left = nullptr, ASTPtr right = nullptr);

    std::string reprBase();
    virtual void visit(ASTVisitor &visitor);
    ASTPtr &left() { return children[0]; }
    ASTPtr &right() { return children[1]; }
};

struct Literal : ASTBase {
    double value;

    Literal(PosT sourcePos, double value);

    std::string reprBase();
    virtual void visit(ASTVisitor &visitor);
};

struct GetVariable : ASTBase {
    std::string variableName;

    GetVariable(PosT sourcePos, std::string variableName);
    std::string reprBase();
    virtual void visit(ASTVisitor &visitor);
};

struct FunctionCall : ASTBase {
    std::string functionName;

    FunctionCall(PosT sourcePos, std::string functionName, std::vector<ASTPtr> arguments = std::vector<ASTPtr>());

    std::string reprBase();
    virtual void visit(ASTVisitor &visitor);
    std::vector<ASTPtr> &arguments() { return children; }
};

struct ASTVisitor {
    virtual void visit(UnaryOperator &op)=0;
    virtual void visit(BinaryOperator &op)=0;
    virtual void visit(Literal &l)=0;
    virtual void visit(FunctionCall &fc)=0;
    virtual void visit(GetVariable &gv)=0;
    virtual ~ASTVisitor() {};
};

#endif // AST_HPP
