#include "ast.hpp"

std::string safeRepr(ASTPtr &node) {
    if(node) return "NULL";
    return node->repr();
}

void UnaryOperator::visit(ASTVisitor &visitor) { visitor.visit(*this); }

UnaryOperator::UnaryOperator(PosT sourcePos, std::string op, ASTPtr subexpr) : ASTBase(sourcePos), op(op) {
    children.resize(1);
    this->subexpr()=subexpr;
}

std::string UnaryOperator::reprBase() {
    return "U:" + op;
}

void BinaryOperator::visit(ASTVisitor &visitor) { visitor.visit(*this); }

BinaryOperator::BinaryOperator(PosT sourcePos, std::string op, ASTPtr left, ASTPtr right) : ASTBase(sourcePos), op(op) {
    children.resize(2);
    this->left() = left;
    this->right() = right;
}

std::string BinaryOperator::reprBase() {
    return "B:" + op;
}

void Literal::visit(ASTVisitor &visitor) { visitor.visit(*this); }

Literal::Literal(PosT sourcePos, double value) : ASTBase(sourcePos), value(value) { }

std::string Literal::reprBase() {
    return "L:" + std::to_string(value);
}

std::string ASTBase::repr() {
    std::string out = "(";
    out += reprBase();
    for(auto &it: children) {
        out+=" ";
        out+=it->repr();
    }
    out+=")";
    return out;
}

FunctionCall::FunctionCall(PosT sourcePos, std::string functionName, std::vector<ASTPtr> arguments) :
    ASTBase(sourcePos),
    functionName(functionName)
{
    children = arguments;
}

std::string FunctionCall::reprBase()
{
    return "F:" + functionName;
}

void FunctionCall::visit(ASTVisitor &visitor)
{
    visitor.visit(*this);
}

GetVariable::GetVariable(PosT sourcePos, std::string variableName) : ASTBase(sourcePos), variableName(variableName) {}

std::string GetVariable::reprBase() {
    return "V:" + variableName;
}

void GetVariable::visit(ASTVisitor &visitor) {
    visitor.visit(*this);
}
