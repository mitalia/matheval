#include "parser.hpp"
#include <ctype.h>
#include <cstring>
#include <unordered_map>
#include <cmath>
#include <vector>
#include "ast.hpp"

static std::unordered_map<std::string, int> priorities;

static int initPriorities() {
    auto p=[&](const std::string &op) -> int& { return priorities[op]; };
    int pri = 0;
    p(",")  = pri++;
    p("||") = pri++;
    p("&&") = pri++;
    p("|")  = pri++;
    p("^")  = pri++;
    p("&")  = pri++;
    p("==") = p("!=") = pri++;
    p("<")  = p("<=") = p(">") = p(">=") = pri++;
    p("<<") = p(">>") = pri++;
    p("+")  = p("-")  = pri++;
    p("*")  = p("/")  = p("%") = pri++;
    p("**") = pri++;
    return pri;
}

static int maxOpPriority = initPriorities();

static int priority(const std::string &tok) {
    if(priorities.count(tok)==0) return -2;
    return priorities[tok];
}

double EvaluatingParser::eval()
{
    nextToken();
    double ret = evalSubexpr(-1);
    if(curTokT!=Eof) err("Unexpected characters at end of expression");
    return ret;
}

void ParserBase::nextToken()
{
    static const char *specials="+-*/%^|&()<>!~=,";
    curToken.clear();
    char c=expr[pos];
    auto nextC = [&]() { c=expr[++pos]; };
    auto goodC = [&]() { curToken+=c; nextC(); };
    while(isspace(c)) nextC();

    if(c==0) {
       curTokT=Eof;
    }
    else if(isalpha(c)) {
        // Valid identifiers:
        // \w[\w\d_]*
        while(isalnum(c) || c=='_') goodC();
        curTokT = Identifier;
    } else if(isdigit(c)) {
        // Valid numbers:
        // \d+(\.\d*){0,1}([Ee][+-]{0,1}\d+){0,1}
        auto eatDigits = [&]() { while(isdigit(c)) goodC(); };
        eatDigits();
        if(c=='.') { goodC(); eatDigits(); }
        if(c=='E' || c=='e') {
            goodC();
            if(c=='+' || c=='-') goodC();
            int curPos=pos;
            eatDigits();
            if(pos==curPos) err("Invalid number: incomplete exponent");
        }
        curTokT = Number;
    } else if(strchr(specials, c)) {
        char cc=c;
        goodC();
        if(cc=='=' && c!='=') err("= alone is not allowed; use == for equality comparison");
        if (
                (cc=='*' && c=='*') ||
                (cc=='<' && c=='=') ||
                (cc=='<' && c=='<') ||
                (cc=='>' && c=='>') ||
                (cc=='>' && c=='=') ||
                (cc=='=' && c=='=') ||
                (cc=='|' && c=='|') ||
                (cc=='&' && c=='&') ||
                (cc=='!' && c=='=')
                ) goodC();
        curTokT = Special;
    } else {
        err("Invalid token");
    }
}

void ParserBase::err(const std::string &message, int pos)
{
    throw ParseError(message, this->expr, pos<0?this->pos-curToken.size():pos);
}

double EvaluatingParser::evalSubexpr(int exprPriority)
{
    if(exprPriority >= maxOpPriority) {
        // Mostly-unary, high priority stuff
        if(curTokT == Number) {
            // Number literals
            double r = atof(curToken.c_str());
            nextToken();
            return r;
        } else if(curTokT == Identifier) {
            int oldpos = pos-curToken.size();
            std::string name = curToken;
            // Variable or function?
            nextToken();
            if(curTokT == Special && curToken == "(") {
                nextToken();
                // Function call
                if(compileContext.functions.count(name)==0) err("Undefined function", oldpos);
                Function &f = *compileContext.functions[name];
                std::vector<double> arguments;
                // Get the arguments
                for(int i=0; i<f.arity; ++i) {
                    // parse subexpression, ignoring comma
                    arguments.push_back(evalSubexpr(1));
                    if(curToken != ((i==f.arity-1)?")":",")) {
                        err("Wrong number of arguments in function call");
                    }
                    nextToken();
                }
                return f.call(&arguments[0]);
            } else {
                // Variable
                if(evalContext.vars.count(name)==0) err("Undefined variable", oldpos);
                return evalContext.vars[name];
            }
        } else if(curTokT == Special) {
            // Unary operators and parentheses
            std::string op=curToken;
            int oldpos=pos-curToken.size();
            nextToken();
            if(op == "(") {
                // Parse the subexpression, starting again with low priorities
                double r = evalSubexpr(-1);
                // check if we got at the end
                if(curToken!=")") err("Unbalanced parenthesis", pos);
                nextToken();
                return r;
            } else if(op=="!") {
                return !evalSubexpr(maxOpPriority);
            } else if(op=="~") {
                return ~unsigned(evalSubexpr(maxOpPriority));
            } else if(op=="+") {
                return evalSubexpr(maxOpPriority);
            } else if(op=="-") {
                return -evalSubexpr(maxOpPriority);
            } else {
                err("Invalid token", oldpos);
            }
        } else {
            err("Expected: number, open parentheses, identifier, unary operator; got `" + curToken + "`");
        }
    }
    // Eat an expression with only higher priority than us
    double l = evalSubexpr(exprPriority + 1);
    // Go on as long as we get operators at the same priority
    while(curTokT == Special) {
        std::string op = curToken;
        int oldpos = pos;
        // We reached something of different priority - return the current result
        if(priority(curToken) != exprPriority) return l;
        nextToken();
        // Get the right operand
        double r = evalSubexpr(exprPriority + 1);
        // Perform the actual calculation
#define R(tok, expr) else if(op==tok) l=(expr)
        if(0);
        R("+", l+r);
        R("-", l-r);
        R("*", l*r);
        R("/", l/r);
        R("%", std::fmod(l, r));
        R("**", std::pow(l, r));
        R("<", l<r);
        R(">", l>r);
        R("<=", l<=r);
        R(">=", l>=r);
        R("==", l>=r);
        R("||", l||r);
        R("&&", l&&r);
        R("<<", unsigned(l)<<unsigned(r));
        R(">>", unsigned(l)>>unsigned(r));
        R("&", unsigned(l)&unsigned(r));
        R("|", unsigned(l)|unsigned(r));
        R(",", r);
        else err("Expected operator", oldpos);
#undef R
    }
    // We should get here only if reach the end of the expression
    return l;
}

EvaluatingParser::EvaluatingParser(const std::string &expr) : ParserBase(expr)
{
}

namespace {
    struct SinF : Function {
        SinF() : Function(1, true) {}
        double call(double args[]) { return std::sin(args[0]); }
    } xSinF;

    struct CosF : Function {
        CosF() : Function(1, true) {}
        double call(double args[]) { return std::cos(args[0]); }
    } xCosF;

    struct TanF : Function {
        TanF() : Function(1, true) {}
        double call(double args[]) { return std::tan(args[0]); }
    } xTanF;

    struct ATan2F : Function {
        ATan2F() : Function(2, true) {}
        double call(double args[]) { return std::atan2(args[0], args[1]); }
    } xATan2F;

    struct PowF : Function {
        PowF() : Function(2, true) {}
        double call(double args[]) { return std::pow(args[0], args[1]); }
    } xPowF;

    struct Log10F : Function {
        Log10F() : Function(1, true) {}
        double call(double args[]) { return std::log10(args[0]); }
    } xLog10F;

    struct LogF : Function {
        LogF() : Function(1, true) {}
        double call(double args[]) { return std::log(args[0]); }
    } xLogF;

    struct ExpF : Function {
        ExpF() : Function(1, true) {}
        double call(double args[]) { return std::exp(args[0]); }
    } xExpF;
}

CompileContext::CompileContext() {
    functions["sin"] = &xSinF;
    functions["cos"] = &xCosF;
    functions["tan"] = &xTanF;
    functions["atan2"] = &xATan2F;
    functions["pow"] = &xPowF;
    functions["log10"] = &xLog10F;
    functions["log"] = &xLogF;
    functions["exp"] = &xExpF;
}

ParserBase::ParserBase(const std::string &expr) : expr(expr) {
}



Function::Function(int arity, bool pure) : arity(arity), pure(pure) {

}

Function::~Function() {

}

ASTPtr ASTParser::evalSubexpr(int exprPriority) {
    if(exprPriority >= maxOpPriority) {
        // Mostly-unary, high priority stuff
        if(curTokT == Number) {
            // Number literals
            ASTPtr r = ASTPtr(new Literal(pos, atof(curToken.c_str())));
            nextToken();
            return r;
        } else if(curTokT == Identifier) {
            int oldpos = pos-curToken.size();
            std::string name = curToken;
            // Variable or function?
            nextToken();
            if(curTokT == Special && curToken == "(") {
                nextToken();
                // Function call
                std::vector<ASTPtr> arguments;
                // Get the arguments
                for(;;) {
                    // parse subexpression, ignoring comma
                    arguments.emplace_back(evalSubexpr(1));
                    if(curToken == ")") break;
                    if(curToken !=",") err("Invalid syntax in function call");
                    nextToken();
                }
                nextToken();
                return ASTPtr(new FunctionCall(oldpos, name, arguments));
            } else {
                // Variable
                return ASTPtr(new GetVariable(oldpos, name));
            }
        } else if(curTokT == Special) {
            // Unary operators and parentheses
            std::string op=curToken;
            int oldpos=pos-curToken.size();
            nextToken();
            if(op=="(") {
                // Parse the subexpression, starting again with low priorities
                ASTPtr r = evalSubexpr(-1);
                // check if we got at the end
                if(curToken!=")") err("Unbalanced parenthesis", pos);
                nextToken();
                return r;
            } else {
                return ASTPtr(new UnaryOperator(oldpos, op, evalSubexpr(maxOpPriority)));
            }
        } else {
            err("Expected: number, open parentheses, identifier, unary operator; got `" + curToken + "`");
        }
    }
    // Eat an expression with only higher priority than us
    ASTPtr l = evalSubexpr(exprPriority + 1);
    // Go on as long as we get operators at the same priority
    while(curTokT == Special) {
        std::string op = curToken;
        int oldpos = pos;
        // We reached something of different priority - return the current result
        if(priority(curToken) != exprPriority) return l;
        nextToken();
        // Get the right operand
        ASTPtr r = evalSubexpr(exprPriority + 1);
        ASTPtr n = ASTPtr(new BinaryOperator(oldpos, op, l, r));
        l = n;
    }
    // We should get here only if reach the end of the expression
    return l;
}

ASTParser::ASTParser(const std::string &expr) : ParserBase(expr) { }

ASTPtr ASTParser::parse() {
    nextToken();
    ASTPtr ret = evalSubexpr(-1);
    if(curTokT!=Eof) err("Unexpected characters at end of expression");
    return ret;
}

namespace {
struct IntASTEval : ASTVisitor {
    ASTEvaluator *parent;
    double val=NAN;

    IntASTEval(ASTEvaluator *parent) : parent(parent) {}

    void err(const std::string &message, int pos) NORETURN {
        throw ParseError(message, parent->expr, pos);
    }

    double eval(ASTPtr ast) {
        ast->visit(*this);
        return val;
    }

    void visit(UnaryOperator &op) {
        auto subexpr = [&] { return IntASTEval(parent).eval(op.subexpr()); };
        if(op.op.size()==1) {
            switch(op.op[0]) {
            case '+': val = +subexpr(); return;
            case '-': val = -subexpr(); return;
            case '!': val = !subexpr(); return;
            case '~': val = ~unsigned(subexpr()); return;
            }
        }
        err("Invalid unary operator: `" + op.op + "`", op.sourcePos);
    }

    void visit(BinaryOperator &op) {
        auto l = [&] { return IntASTEval(parent).eval(op.left()); };
        auto r = [&] { return IntASTEval(parent).eval(op.right()); };
#define R(tok, expr) else if(op.op==tok) val=(expr)
        if(0);
        R("+", l()+r());
        R("-", l()-r());
        R("*", l()*r());
        R("/", l()/r());
        R("%", std::fmod(l(), r()));
        R("**", std::pow(l(), r()));
        R("<", l()<r());
        R(">", l()>r());
        R("<=", l()<=r());
        R(">=", l()>=r());
        R("==", l()>=r());
        R("||", l()||r());
        R("&&", l()&&r());
        R("<<", unsigned(l())<<unsigned(r()));
        R(">>", unsigned(l())>>unsigned(r()));
        R("&", unsigned(l())&unsigned(r()));
        R("|", unsigned(l())|unsigned(r()));
        R(",", r());
        else err("Invalid binary operator: `" + op.op + "`", op.sourcePos);
#undef R
    }

    void visit(Literal &l) {
        val = l.value;
    }

    void visit(FunctionCall &fc) {
        auto &fns = parent->compileContext.functions;
        if(fns.count(fc.functionName)==0) err("Undefined function: `" + fc.functionName + "`", fc.sourcePos);
        Function *fn = fns[fc.functionName];
        if(fn->arity!=int(fc.arguments().size())) {
            err("Invalid number of arguments to function `" + fc.functionName + "`:"
                " expected " + std::to_string(fn->arity) + ", got " + std::to_string(fc.children.size()),
                fc.sourcePos);
        }
        std::vector<double> args;
        for(auto arg: fc.arguments()) {
            args.push_back(IntASTEval(parent).eval(arg));
        }
        val = fn->call(args.data());
    }

    void visit(GetVariable &gv) {
        auto &vars = parent->evalContext.vars;
        if(vars.count(gv.variableName)==0) err("Undefined variable: `" + gv.variableName + "`", gv.sourcePos);
        val = vars[gv.variableName];
    }
};
}

double ASTEvaluator::eval() {
    IntASTEval e(this);
    return e.eval(this->ast);
}

double RunContext::run() {
    double *s=&stack[0];

    uint8_t *p = &(code->code[0]), *e = p + code->code.size();
    auto l_double = [&]() {
        double ret;
        memcpy(&ret, p, sizeof(ret));
        p+=sizeof(ret);
        return ret;
    };
    auto l_uint16 = [&]() {
        uint16_t ret;
        memcpy(&ret, p, sizeof(ret));
        p+=sizeof(ret);
        return ret;
    };
#define R(op) {\
    double r = *s--;\
    double l = *s;\
    double res = op;\
    *s=res;\
    } break

    for(;p<e;) {
        switch(*p++) {
        case Nop:       break;
        case Pop:       s--; break;
        case Load:      *++s = l_double(); break;
        case LoadVar:   *++s=vars.at(l_uint16()); break;
        case CallFn: {
            Function *fn = code->funcs[l_uint16()];
            double *args = s-fn->arity+1;
            double ret = fn->call(args);
            s-=fn->arity;
            *++s=ret;
            break;
        }
        case Neg:       *s = -*s; break;
        case Add:       R(l+r);
        case Sub:       R(l-r);
        case Mul:       R(l*r);
        case Div:       R(l/r);
        case Mod:       R(fmod(l,r));
        case Pow:       R(pow(l,r));
        case And:       R(unsigned(l)&unsigned(r));
        case Or:        R(unsigned(l)|unsigned(r));
        case Xor:       R(unsigned(l)^unsigned(r));
        case Not:       *s = ~unsigned(*s); break;
        case LNot:      *s = !*s; break;
        case LAnd:      R(l&&r);
        case LOr:       R(l||r);
        case Lt:        R(l<r);
        case Le:        R(l<=r);
        case Eq:        R(l==r);
        case Ge:        R(l>=r);
        case Gt:        R(l>r);
        case Ne:        R(l!=r);
        case Shl:       R(unsigned(l)<<unsigned(r));
        case Shr:       R(unsigned(l)>>unsigned(r));
        default:
            throw std::runtime_error("Invalid opcode");
        }
    }
#undef R
    if(s!=&stack[1]) throw std::runtime_error("Unbalanced stack");
    return *s;
}

CodePtr ASTCompiler::compile() {
    stack_size = 0;
    out.reset(new CompiledCode);
    ast->visit(*this);
    CodePtr ret(out);
    out.reset();
    return ret;
}

void ASTCompiler::visit(UnaryOperator &op) {
    op.subexpr()->visit(*this);
    if(op.op.size()==1) {
        switch(op.op[0]) {
        case '+': return;
        case '-': push_code(Neg); return;
        case '!': push_code(LNot); return;
        case '~': push_code(Not); return;
        }
    }
    err("Invalid unary operator: `" + op.op + "`", op.sourcePos);
}

void ASTCompiler::visit(BinaryOperator &op) {
    static std::unordered_map<std::string, Opcode> op2code = {
        {"+", Add},
        {"-", Sub},
        {"*", Mul},
        {"/", Div},
        {"%", Mod},
        {"**", Pow},
        {"&", And},
        {"|", Or},
        {"^", Xor},
        {"&&", LAnd},
        {"||", LOr},
        {"<", Lt},
        {"<=", Le},
        {"==", Eq},
        {">=", Ge},
        {">", Gt},
        {"!=", Ne},
        {"<<", Shl},
        {">>", Shr}
    };
    if(op.op==",") {
        op.right()->visit(*this);
        return;
    }
    if(op2code.count(op.op)==0) {
        err("Invalid binary operator: `" + op.op + "`", op.sourcePos);
    }
    op.left()->visit(*this);
    op.right()->visit(*this);
    push_code(op2code[op.op]);
    update_stack_size(-1);
}

void ASTCompiler::visit(Literal &l) {
    push_code(Load);
    push_val(l.value);
    update_stack_size(1);
}

void ASTCompiler::visit(FunctionCall &fc) {
    auto &fns = compileContext.functions;
    if(fns.count(fc.functionName)==0) err("Undefined function: `" + fc.functionName + "`", fc.sourcePos);
    Function *fn = fns[fc.functionName];
    if(fn->arity!=int(fc.arguments().size())) {
        err("Invalid number of arguments to function `" + fc.functionName + "`:"
                                                                            " expected " + std::to_string(fn->arity) + ", got " + std::to_string(fc.children.size()),
            fc.sourcePos);
    }
    for(auto arg: fc.arguments()) {
        arg->visit(*this);
    }
    push_code(CallFn);
    uint16_t fnid = out->funcs.size();
    if(fnmap.count(fc.functionName)) {
        fnid = fnmap[fc.functionName];
    } else {
        fnmap[fc.functionName] = fnid;
        out->funcs.push_back(fns[fc.functionName]);
    }
    push_uint16(fnid);
    update_stack_size(-fc.arguments().size()+1);
}

void ASTCompiler::visit(GetVariable &gv) {
    push_code(LoadVar);
    uint16_t varid = out->var_ids.size();
    if(varmap.count(gv.variableName)) {
        varid = varmap[gv.variableName];
    } else {
        varmap[gv.variableName] = varid;
        out->var_ids.push_back(gv.variableName);
    }
    push_uint16(varid);
    update_stack_size(1);
}

void ASTCompiler::push_code(Opcode opcode)
{
    out->code.push_back(opcode);
}

void ASTCompiler::push_val(double val)
{
    out->code.insert(out->code.end(),
                    (uint8_t *)&val, (uint8_t *)(&val + 1));
}

void ASTCompiler::push_uint16(uint16_t val)
{
    out->code.insert(out->code.end(),
                     (uint8_t *)&val, (uint8_t *)(&val + 1));
}

void ASTCompiler::update_stack_size(int inc)
{
    stack_size += inc;
    if(stack_size > out->stack_size) {
        out->stack_size = stack_size;
    }
}

RunContext::RunContext(const EvalContext &ctx, CodePtr code)
    : code(code)
{
    stack.resize(code->stack_size+1);
    vars.resize(code->var_ids.size());
    for(int i=0, n=vars.size(); i<n; ++i) {
        std::string name = code->var_ids[i];
        auto it = ctx.vars.find(name);
        if(it == ctx.vars.end()) {
            throw std::runtime_error("Missing variable `" + name + "`");
        }
        name2id[name] = i;
        vars[i] = it->second;
    }
}
