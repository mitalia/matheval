#include <iostream>
#include "matheval/parser.hpp"
#include <cmath>
#include <chrono>
#include <stdio.h>
#include "stopwatch.hpp"

const int repeats = 1000;

int main() {
    EvalContext context;
    context.vars["e"] = std::exp(1.);
    context.vars["pi"] = 3.14159265359;
    std::string s;
    while(std::getline(std::cin, s)) {
        Stopwatch sw;
        try {
            std::cout<<"--- EvaluatingParser ---\n";
            sw.click();
            double val = EvaluatingParser::eval(s, context);
            sw.print_click("eval");
            std::cout<<val<<"\n";
        } catch(const ParseError &ex) {
            std::cout<<ex.what()<<"\n";
        }
        try {
            std::cout<<"--- ASTParser ---\n";
            ASTParser p(s);
            sw.click();
            ASTPtr ast = p.parse();
            sw.print_click("AST parse");
            std::cout<<ast->repr()<<"\n";

            try {
                std::cout<<"--- ASTEvaluator ---\n";
                ASTEvaluator eval(ast, s);
                eval.evalContext = context;
                double ret;
                sw.click();
                for(int i=0; i<repeats; ++i) {
                    ret = eval.eval();
                }
                sw.print_click("AST eval", repeats);
                std::cout<<ret<<"\n";
            } catch(const ParseError &ex) {
                std::cout<<ex.what()<<"\n";
            }

            try {
                std::cout<<"--- ASTCompiler ---\n";
                ASTCompiler compiler(ast, s);
                sw.click();
                RunContext r(context, compiler.compile());
                sw.print_click("AST compile");
                sw.click();
                double ret;
                for(int i=0; i<repeats; ++i) {
                    ret = r.run();
                }
                sw.print_click("Compiled run", repeats);
                std::cout<<ret<<"\n";
            } catch(const std::runtime_error &ex) {
                std::cout<<ex.what()<<"\n";
            }
        } catch(const std::runtime_error &ex) {
            std::cout<<ex.what()<<"\n";
        }
    }
    return 0;
}
