add_executable(comparison comparison.cpp stopwatch.hpp)
target_link_libraries(comparison libmatheval)

add_executable(2d 2d.cpp)
target_link_libraries(2d libmatheval)

add_executable(shortcircuitbug shortcircuitbug.cpp)
target_link_libraries(shortcircuitbug libmatheval)
