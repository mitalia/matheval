#include "matheval/ast.hpp"
#include "matheval/parser.hpp"
#include <stdio.h>

int main() {
    struct PrintF : Function {
        PrintF() : Function(1, false) {}
        double call(double args[]) { printf("%f\n", args[0]); return 0; }
    } xPrintF;

    std::string expr = "(print(1)+1) || print(0)";
    CompileContext cctx;
    cctx.functions["print"] = &xPrintF;
    EvaluatingParser ep(expr);
    ep.compileContext = cctx;
    ep.eval();
    puts("---");

    ASTPtr ast = ASTParser(expr).parse();
    ASTEvaluator av(ast, expr);
    av.compileContext = cctx;
    av.eval();
    puts("---");

    ASTCompiler compiler(ast, expr);
    compiler.compileContext = cctx;
    RunContext rc(EvalContext(), compiler.compile());
    rc.run();
    puts("---");
    return 0;
}
