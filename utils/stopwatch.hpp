#ifndef STOPWATCH_HPP_INCLUDED
#define STOPWATCH_HPP_INCLUDED

#include <chrono>
#include <stdio.h>

struct Stopwatch {
    std::chrono::high_resolution_clock::time_point last;

    Stopwatch() { click(); }

    double click() {
        auto now = std::chrono::high_resolution_clock::now();
        double ret = std::chrono::duration_cast<std::chrono::duration<double>>(now-last) / std::chrono::duration<double>(1);
        last = now;
        return ret;
    }

    void print_click(const char *str=nullptr, int repeats = 1) {
        double t = click()*1000/repeats;
        if(str) printf("%s: ", str);
        printf("%.04f ms ", t);
        if(repeats!=1) printf("(average of %d) ", repeats);
        putchar('\n');
        click();
    }
};

#endif
