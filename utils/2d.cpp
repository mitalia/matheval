#include "matheval/ast.hpp"
#include "matheval/parser.hpp"
#include <iostream>
#include <cmath>
#include <stdio.h>

const int w = 1024;
const int h = 1024;

int main(int argc, char *argv[]) {
    if(argc<2) {
        std::cerr<<"Not enough arguments\n";
    }
    std::string expr = argv[1];
    EvalContext ctx;
    ctx.vars["e"] = std::exp(1);
    ctx.vars["pi"] = 3.14159265359;
    ctx.vars["w"] = w;
    ctx.vars["h"] = h;
    ctx.vars["maxval"] = 255;
    ctx.vars["x"] = 0;
    ctx.vars["y"] = 0;
    try {
        ASTPtr ast = ASTParser(expr).parse();
        RunContext r(ctx, ASTCompiler(ast, expr).compile());
        std::vector<unsigned char> img(w*h);
        int xid = r.varId("x");
        int yid = r.varId("y");
        for(int y=0; y<h; ++y) {
            if(yid>=0) r.vars[yid] = y;
            for(int x=0; x<w; ++x) {
                if(xid>=0) r.vars[xid] = x;
                img[y*w+x] = r.run();
            }
        }
        printf("P5\n%d %d\n255\n", w, h);
        fwrite(&img[0], img.size(), 1, stdout);
    } catch(const std::runtime_error &ex) {
        std::cerr<<ex.what()<<"\n";
        return 1;
    }
}
