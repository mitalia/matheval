# MathEval - some math parsers #

Testbed for mathematical parsers/evaluators implementations.

### Features ###

- all calculations performed with `double`s (but: bitwise operators implemented with casts to `unsigned`);
- off-the-shelf, C-like operators (with `**` thrown in for good measure; `&&` and `||` short-circuit in some implementations);
- read-only variables (AKA constants);
- external functions with arbitrary arity.

All the parsers support mostly the same feature set, and examine the text using a recursive descent parser; currently we have:

- `EvaluatingParser` - parses and evaluates at the same time; good for throwaway calculations;
- `ASTParser` - parses an expression and generates an AST, good for inspection, transformation and further compilation;
- `ASTEvaluator` - takes an AST and evaluates it with the given set of constants/functions; mainly a proof of concept to test the correctness of the `ASTParser`, will be useful to perform simplifications on the AST;
- `ASTCompiler` - takes an AST and compiles it to bytecode for a simple, reasonably fast stack-based VM;
- `RunContext` - encapsulates the context needed to run the code (the variables values, the name -> index mapping and the actual bytecode) and allows to run it.